#Angular Technical Challenge

Set up an angular material (https://material.angular.io/) project to provide CRUD functionality for widgets. Widgets should have an id, title, date, price and description. There should be separate routes for the list and detail pages. 

The list page should utilize an angular material data table with a local datasource. It should be sortable, searchable, paginated, and contain links to a detail page for each widget. The detail page page should use appropriate angular material components (e.g. date selectors for the date field).

Bonus challenges: 

add a core service for the app to notify users that a widget has been created, updated or deleted.
incorporate a rich text editor for the widget description field.
