import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {WidgetsComponent} from './components/widgets/widgets.component';
import {WidgetsDetailComponent} from './components/widgets-detail/widgets-detail.component';
import {WidgetDetailsResolver} from './components/widgets-detail/widget-details.resolver';

const routes: Routes = [
  { path: '',
  redirectTo: '/home',
  pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent,
    data: { title: 'Demo Home' }
  },
  {
    path: 'widgets',
    component: WidgetsComponent,
    data: { title: 'Widgets List' }
  },
  {
    path: 'widgets/:id',
    component: WidgetsDetailComponent,
    data: { title: 'Widget Detail' },
    resolve: {
      widget: WidgetDetailsResolver
    }
  }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
