import { Injectable } from '@angular/core';
import {Widget} from '../models/widget';

@Injectable({
  providedIn: 'root'
})
export class WidgetService {

  widgets: Widget[] = [
    {
      id: 1,
      title: 'Widget 1',
      date: new Date(),
      price: '1.99',
      descriptionHTML: 'Widget 1 description'
    },
    {
      id: 2,
      title: 'Widget 2',
      date: new Date(),
      price: '2.99',
      descriptionHTML: 'Widget 2 description'
    },
    {
      id: 3,
      title: 'Widget 3',
      date: new Date(),
      price: '3.99',
      descriptionHTML: 'Widget 3 description'
    },
    {
      id: 4,
      title: 'Widget 4',
      date: new Date(),
      price: '4.99',
      descriptionHTML: 'Widget 4 description'
    },
    {
      id: 5,
      title: 'Widget 5',
      date: new Date(),
      price: '5.99',
      descriptionHTML: 'Widget 5 description'
    },
    {
      id: 6,
      title: 'Widget 6',
      date: new Date(),
      price: '6.99',
      descriptionHTML: 'Widget 6 description'
    },
    {
      id: 7,
      title: 'Widget 7',
      date: new Date(),
      price: '7.99',
      descriptionHTML: 'Widget 7 description'
    },
    {
      id: 8,
      title: 'Widget 8',
      date: new Date(),
      price: '8.99',
      descriptionHTML: 'Widget 8 description'
    },
    {
      id: 9,
      title: 'Widget 9',
      date: new Date(),
      price: '9.99',
      descriptionHTML: 'Widget 9 description'
    },
    {
      id: 10,
      title: 'Widget 10',
      date: new Date(),
      price: '10.99',
      descriptionHTML: 'Widget 10 description'
    }
  ];

  constructor() { }


  getById(id: number): Widget {
    console.log('getById : ', id);
    return this.widgets.find( (widget: Widget) => {
      console.log('widget: ', widget);
      return widget.id == id;
    });

  }
}
