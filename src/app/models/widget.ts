export interface Widget {
  id: number;
  title: string;
  date: Date;
  price: string;
  descriptionHTML: string;
  descriptionRawText?: string;
}
