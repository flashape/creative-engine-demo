import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {WidgetService} from '../../services/widget.service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Widget} from '../../models/widget';

@Component({
  selector: 'app-widgets',
  templateUrl: './widgets.component.html',
  styleUrls: ['./widgets.component.css']
})
export class WidgetsComponent implements OnInit, AfterViewInit {

  gridColumns = ['title', 'price', 'date', 'description'];
  dataSource: MatTableDataSource<Widget>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public widgetService: WidgetService) {
    this.dataSource = new MatTableDataSource<Widget>(this.widgetService.widgets);

  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
