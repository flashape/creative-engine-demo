import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {Widget} from '../../models/widget';
import {WidgetService} from '../../services/widget.service';


@Injectable()
export class WidgetDetailsResolver implements Resolve<any>{

  constructor(private widgetService: WidgetService) {
    console.log('new WidgetDetailsResolver()');
  }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Widget {

      console.log('route.params.id : ', route.params.id);
    const widget: Widget = this.widgetService.getById(route.params.id);

    console.log('WidgetDetailsResolver FOUND WIDGET : ', widget);

    return widget;
  }

}
