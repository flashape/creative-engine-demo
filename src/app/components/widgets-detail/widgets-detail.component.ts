import { Component, OnInit } from '@angular/core';
import {Widget} from '../../models/widget';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-widgets-detail',
  templateUrl: './widgets-detail.component.html',
  styleUrls: ['./widgets-detail.component.css']
})
export class WidgetsDetailComponent implements OnInit {

  widget: Widget;
  editorModules: any = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
      ['blockquote', 'code-block'],

      [{ 'list': 'ordered'}, { 'list': 'bullet' }],

      [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

      [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
      [{ 'font': [] }],
      [{ 'align': [] }],

      ['clean'],                                         // remove formatting button

    ]
  };

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe( (data: {widget: Widget}) =>{
      this.widget = data.widget;
    });
  }

  onDescriptionChanged(info: any) {
    // info : { content: any; delta: any; editor: any; html: string | null; oldDelta: any; source: string; text: string }
    this.widget.descriptionRawText = info.text;
  }
}
